#  Copyright (C) 2020
#  Laurent Pater <laurent.pater@ens-cachan.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 3 or
#  above as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#  Ce programme permet d'extraire des données texte de la Webstation
#  du logiciel Clinisoft TM
#  https://www.gehealthcare.fr/products/healthcare-it/centricity-high-acuity-critical-care
#  On peut ensuite travailler directement sur ces données très légères
from selenium import webdriver
import time
driver = webdriver.Chrome('/usr/bin/chromedriver')
import os
import datetime
#A l'exécution du script, le driver de chromium ouvre une fenetre ce qui permet d'indiquer identifiant et
#mot de passe à clinisoft01/webstation/


#On indique ici le dossier pour placer les fichiers des fichiers html
path_save='mon_dossier/'

#temps d'attente en secondes pour le chargement de chaque page (a priori, 2.5 secondes suffit pour la plupart des fichiers)
#il semble qu'on puisse meme parfois réduire
waiting_time = 2.5

#fonction d'enregistrement d'une page web avec selenium
def enregistrer_page(adresse,output):
    with open(path_save+output, 'w') as f:
        page=driver.get(adresse)
        time.sleep(waiting_time)
        f.write(driver.page_source)

#Dictionnaire des patients établi en indiquant le numéro du patient dans l'étude et son code Clinisoft
#Dans l'ordre, on a le code Clinisoft, la date d'entrée, la "ward_id" (lié à l'unité d'accueil) et la date de sortie. Les deux dates sont légèrement étendues

dictionnaire_patient=dict()
dictionnaire_patient[8]=(2457,datetime.datetime(2020,4,22),427,datetime.datetime(2020,4,3))
dictionnaire_patient[25]=(25485,datetime.datetime(2020,3,30),427,datetime.datetime(2020,4,8))

##Telechargement de labos
#Ici on présente la fonction pour télécharger une partie des données labo
#le fait de s'etre limitée est basée sur l'étude clinique durant laquelle ce script a été conçu
#on peut étendre à l'ensemble des données

#adresses de la webstation retrouvées
x='http://clinisoft01/webstation/ic_report/bericht_ausgabe_universal.php?pat_id='
y='&type=TXT_REPORT_PIECE_MOD1_IT2&title=Labor+%2F+'
z='&wardid='
#la partie warehouse ne semble pas avoir de sens pour le labo
zz='&warehouse=0&winwidth=1066'

def telecharger_labo_patient(numpatient):
    date=datetime.datetime.today()
    str_date=date.strftime("%Y_%m_%d")
    type(str(date))
    c_clini=str(dictionnaire_patient[numpatient][0])
    warehouse=str(dictionnaire_patient[numpatient][2])
    nom_fichiers_sortie=str(numpatient)+'_'+c_clini+'_'+str_date
    print(nom_fichiers_sortie)
    #on présente ici à la suite les différentes adresses trouvées à partir de la web station avec "afficher le contenu du cadre"
    enregistrer_page(x+c_clini+y+'%20%20Gaz%20du%20Sang&seq_id=48&name=Labor+%2F+%20%20Gaz%20du%20Sang'+z+warehouse+zz,nom_fichiers_sortie+'_labo01.html')
    enregistrer_page(x+c_clini+y+'%20%20H%E9mostase&seq_id=69&name=Labor+%2F+%20%20H%E9mostase'+z+warehouse+zz,nom_fichiers_sortie+'_labo02.html')
    enregistrer_page(x+c_clini+y+'%20%20Ionogramme&seq_id=76&name=Labor+%2F+%20%20Ionogramme'+z+warehouse+zz,nom_fichiers_sortie+'_labo03.html')
    enregistrer_page(x+c_clini+y+'%20%20Num%E9ration&seq_id=1&name=Labor+%2F+%20%20Num%E9ration'+z+warehouse+zz,nom_fichiers_sortie+'_labo04.html')
    enregistrer_page(x+c_clini+y+'%20Dosage%20ATB&seq_id=75&name=Labor+%2F+%20Dosage%20ATB'+z+warehouse+zz,nom_fichiers_sortie+'_labo05.html')
    enregistrer_page(x+c_clini+y+'%20H%E9patique&seq_id=37&name=Labor+%2F+%20H%E9patique'+z+warehouse+zz,nom_fichiers_sortie+'_labo06.html')
    enregistrer_page(x+c_clini+y+'%20H%E9patique&seq_id=37&name=Labor+%2F+%20H%E9patique'+z+warehouse+zz,nom_fichiers_sortie+'_labo07.html')
    enregistrer_page(x+c_clini+y+'Bilan%20enzymatiq.coeur-pancr%E9as&seq_id=38&name=Labor+%2F+Bilan%20enzymatiq.coeur-pancr%E9as'+z+warehouse+zz,nom_fichiers_sortie+'_labo08.html')
    enregistrer_page(x+c_clini+y+'Bilan%20thyroidien%20minimum&seq_id=52&name=Labor+%2F+Bilan%20thyroidien%20minimum'+z+warehouse+zz,nom_fichiers_sortie+'_labo09.html')
    enregistrer_page(x+c_clini+y+'Ponction%20LCR&seq_id=77&name=Labor+%2F+Ponction%20LCR'+z+warehouse+zz,nom_fichiers_sortie+'_labo10.html')
    enregistrer_page(x+c_clini+y+'Ponction%20LCR&seq_id=77&name=Labor+%2F+Ponction%20LCR'+z+warehouse+zz,nom_fichiers_sortie+'_labo11.html')
    enregistrer_page(x+c_clini+y+'Web%20Gaz%20du%20sang&seq_id=78&name=Labor+%2F+Web%20Gaz%20du%20sang'+z+warehouse+zz,nom_fichiers_sortie+'_labo12.html')

#cette fonction permet de télécharger les données labo pour l'ensemble des patients
def telecharger_labo():
    for k in list(dictionnaire_patient):
        telecharger_labo_patient(k)

##Telechargement patient jour fixe
#On présente ici le téléchargement de 4 pages de données patients : le dossier médical, les infections nosocomiales (intéressantes pour les entrées sorties d'éléments du patient), le dossier paramédical le résumé des commentaires et les éléments de soins
#On peut étendre ceci à d'autres données
a=['http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title=00_DOSSIER_MEDICAL&pat_id=','http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title=00_INFECTIONS+NOSOCOMIALES&pat_id=','http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title=00_Observations+Param%E9dicales&pat_id=','http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title=09_COMMENTS_ALL&pat_id=','http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title=11_SEQUENCIES&pat_id=']
b=['&name=Observations+m%E9dicales&wardid=','&name=Infections+Nosocomiales&wardid=','&name=Observations+Param%E9dicales&wardid=','&name=R%E9sum%E9+des+commentaires&wardid=','&name=Elements+de+soins&wardid=']
#A nouveau le warehouse ne semble pas nécessaire
c='&warehouse=0&winwidth=1066'
def telecharger_fixe_patient(numpatient):
    date=datetime.datetime.today()
    str_date=date.strftime("%Y_%m_%d")
    c_clini=str(dictionnaire_patient[numpatient][0])
    warehouse=str(dictionnaire_patient[numpatient][2])
    nom_fichiers_sortie=str(numpatient)+'_'+c_clini+'_'+str_date
    print(nom_fichiers_sortie)
    for k in range(5):
        enregistrer_page(a[k]+c_clini+b[k]+warehouse+c,nom_fichiers_sortie+'_'+str(k)+'.html')

#cette fonction permet de télécharger l'ensemble des données "fixes" du patient
def telecharger_fixe():
    for k in list(dictionnaire_patient):
        telecharger_fixe_patient(k)

##Telechargement jour par jour
#on présente ici une fonction pour télécharger les données présentées de façon quotidienne sur la webstation : on peut choisir la plage de 24h souhaitée avec '++9%3A00AM'qui donne de 10h du matin à 9h59 du lendemain. On se limite aux scores, aux données cardio vasculaires et aux médicaments comme dans l'étude relative à la conception de ce script
u0='http://clinisoft01/webstation/ic_report/bericht_ausgabe.php?title='
u=['00_Scores+R%E9animation&pat_id=','04_BILANCE&pat_id=','05_MONITORED&pat_id=','08_MEDICATION_24&pat_id=','17_LABOR&pat_id=']
v=['&name=Scores+r%E9animation&wardid=','&name=Drainage+%26+balance&wardid=','&name=Cardiovasculaire+%26+Respiratoire&wardid=','&name=Traitements+m%E9dicamenteux+%26+liquidiens+24+h&wardid=','&name=R%E9sultats+Examens+Biologiques+%2F+7J&wardid=']
v0='&warehouse=0&winwidth=1066&cursor_end_date='

#Cette fonction est nécessaire pour la passage du format "datetime" de Python au format date de la requête SQL de la webstation : en effet le format Python permet des opérations sur ces objets ce que ne permet pas le format clinisoft. La fonction est conçue pour mars, avril et mai mais une extension est bien sûr possible. Le choix de faire des tableaux de 10h à 9h59 est également lié à l'étude
def formatdate(date):
    x = date.strftime("%Y%m%d")
    annee=x[0:4]
    mois=int(x[4:6])
    if mois==3:
        mois='Mar'
    if mois==4:
        mois='Apr'
    if mois==5:
        mois='May'
    jour=str(int(x[6:8]))
    chaine=mois+'+'+jour+'+'+annee+'++9%3A00AM'
    return(chaine)

#cette fonction télécharge les données dynamiques du patient : il est à noter qu'on prend de la marge avec les jours de présence du patient
def telechargement_dynamique(numpatient,section,fin):
    date_debut = dictionnaire_patient[numpatient][1]- datetime.timedelta(days=2)
    date_jour = datetime.datetime.today()
    c_clini=str(dictionnaire_patient[numpatient][0])
    warehouse=str(dictionnaire_patient[numpatient][2])
    #ici on limite les téléchargement à la période définie avec le dictionnaire
    while date_debut < fin:
        nom_fichiers_sortie=str(numpatient)+'_'+c_clini+'_'+str(section)+'_'+date_debut.strftime("%Y_%m_%d")
        print(nom_fichiers_sortie)
        enregistrer_page(u0+u[section]+c_clini+v[section]+warehouse+v0+formatdate(date_debut),nom_fichiers_sortie+'.html')
        date_debut += datetime.timedelta(days=1)

#cette fonction permet de télécharger tous les patients.
def telecharger_dyn():
    for i in list(dictionnaire_patient):
        fin=dictionnaire_patient[i][-1]
        for j in range(4):
            telechargement_dynamique(i,j,fin)

##Complet
#Cette fonction télécharge toutes les données de tous les patients souhaités.
def telechargement_complet():
    telecharger_fixe()
    telecharger_labo()
    telecharger_dyn()

#pour tester il suffit d'exécuter telechargement_complet()