#  Copyright (C) 2020
#  Laurent Pater <laurent.pater@ens-cachan.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 3 or
#  above as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#  Ce programme permet, lorsque des données ont été extraites de la Webstation du logiciel Clinisoft TM
#  https://www.gehealthcare.fr/products/healthcare-it/centricity-high-acuity-critical-care
#  de transformer les scores de réanimation et les données cardiovasculaires en tableau csv
#  Ce script est en interaction avec le script clinisoft2html des mêmes auteurs.
import re
import os
import csv
import bs4 as bs
import time
import sys
import shutil
#dossier contenant les données à traiter sinon, là où est le code lorsqu'il est exécuté en dehors d'un interpréteur
path='dossier_a_choisir_pour_les_fichiers'
#path = os.path.dirname(os.path.realpath(__file__))+'/'
##Conversion en utf8 : non nécessaire la plupart du temps lorsque les fichiers sont téléchargés avec Sélénium
os.chdir(path)
#la liste des fichiers comprend l'intégralité des fichiers html à utiliser
#on prendra garde de ne prendre que les fichiers de score et de données cardiovasculaires
#La syntaxe pour ces fichiers est la suivante (en utilisant le clinisoft2html) : 1_18346_0_2020_04_01 [numero_dans l'etude]_[codeclinisoft]_[0 les scores, 2 pour le cardiovasculaire]_[date au format anglo-saxon avec _] A noter que le 1 a été réservé pour les évolutions de la balance qui pourra faire l'objet d'un autre code
liste_fichiers = [x for x in liste_fichiers if x[-4:] == 'html']
#on exécute cette fonction ou on copie à la main les fichiers souhaités on peut aussi utilisé la fonction suivante qui ne va copier que les fichiers scores ou cardio dans le dossier converted

#cette fonction est spécifique Linux et il faudrait en faire un multiplateformes
def conversion_utf8():
    for x in liste_fichiers:
        os.system('iconv -f ISO-8859-1 -t UTF-8 '+x+' -o '+path+'converted/'+x[:-5])

##Ici une fonction qui fait uniquement une copie des fichiers à traiter
def sans_conversion(patient):
    if not os.path.exists('converted'):
        os.makedirs('converted')
    liste_fichiers=os.listdir()
    liste_fichiers = [x for x in liste_fichiers if x[-4:] == 'html' and str(patient) in x]
    for x in liste_fichiers:
        if '_0_' in x or '_2_' in x:
            shutil.copy(x,path+'converted/'+x)
            #version Linux plus efficace
            #os.system('cp '+x+' '+path+'converted/'+x)

##conversion en CSV
#fonction de concatenation de csv (sans doute obsolete et a modifier avec des modules Python peut-être un peut plus efficaces. Méthode : transposer sous forme matricielle, concaténer puis transposer de nouveau.
def concatenation(first,second,output):
    with open(first) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if L==[]:
            a=0
        else:
            a = max([len(x) for x in L])
        for row in L:
            #on remplit ici les lignes pour qu'elles aient toutes la même longueur
            while len(row) <a:
                row.append('')
        L_trans01=list(map(list, zip(*L)))
    with open(second) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if L==[]:
            b=0
        else:
            b=max([len(x) for x in L])
        for row in L:
            while len(row) <b:
                row.append('')
        L_trans02=list(map(list, zip(*L)))
    L_result=L_trans01+L_trans02
    if L_result==[]:
        c=0
    else:
        c = max([len(x) for x in L_result])
    for x in L_result:
        while len(x) < c:
            x.append('')
    L_result=list(map(list, zip(*L_result)))
    with open(output, 'w', newline='') as myfile:
        wr = csv.writer(myfile)
        for row in L_result:
            wr.writerow(row)

def doublestableaux2csv(adress):
    """Retourne une liste du tableau csv complet sur une journée (si complète)"""
    source = open(adress,'r')
    #le strainer permet de limiter la lecture lors des fichiers et accélérer le parsing
    #le module bs4 est nécessaire mais performant
    #Beautiful Soup est un parser efficace mais il serait sans doute possible de faire mieux
    #(en temps de calcul) en créant un parseur à la main pour notre cas particulier
    strainer = bs.SoupStrainer('table')
    soup = bs.BeautifulSoup(source,'lxml',parse_only=strainer,from_encoding="UTF-8")
    tables = soup.find_all("tbody")
    if len(tables) == 0:
        return([])
    table = tables[0]
    table_rows = table.find_all('tr')
    tableau_complet=[]
    for tr in table_rows:
        td = tr.find_all('td')
        row = [i.text for i in td]
        tableau_complet.append(row)
    if len(tables) == 2:
        table = tables[1]
        table_rows = table.find_all('tr')
        for k in range(len(table_rows)):
            td = table_rows[k].find_all('td')
            row = [i.text for i in td]
            for data in row:
                tableau_complet[k].append(data)
    with open(adress, "r",encoding="utf8", errors='ignore') as f:
        L=[]
        lines = f.readlines()
        #recherche des dates et créneau horaire : important pour un horodatage précis si erreur
        for _ in lines:
            if '<div style="padding: 4px 0px 0px 4px;"><span class="blue12bold">' in _:
                chaine=re.search('<div style="padding: 4px 0px 0px 4px;"><span class="blue12bold">(.*)',_).group(1)
                chaine=chaine[:35]
                L.append(chaine)
        if len(L)==0:
            ligne_tete=[[]]
        if len(L) == 1:
            ligne_tete=[['']+[L[0]]+['']*11]
        if len(L) == 2:
            ligne_tete=[['']+[L[0]]+['']*11+['']+[L[1]]+['']*11]
    return(ligne_tete+tableau_complet)

def concatenation_par_jour(patient,mode):
    #nouveau passage pour se placer dans les bons dossiers et les bons fichiers
    os.chdir(path+'converted/')
    if not os.path.exists('csv'):
        os.makedirs('csv')
    liste_fichiers=os.listdir()
    liste_fichiers = [x for x in liste_fichiers if x[-4:] == 'html' and str(patient) in x]
    liste_fichiers.sort()
    affiche=liste_fichiers[0][0:2]
    for x in liste_fichiers:
        #pour afficher le patient en cours (numéro dans l'étude)
        if x[0:2] != affiche:
            print(x[0:2])
            affiche=x[0:2]
        if '_'+str(mode)+'_' in x:
            tableau_date=doublestableaux2csv(x)
            with open('csv/'+x+'.csv', 'w', newline='') as myfile:
                wr = csv.writer(myfile)
                for row in tableau_date:
                    wr.writerow(row)

##Concatenation des jours
def concatenation_par_patient(patient,mode):
    os.chdir(path+'converted/csv')
    liste_fichiers=os.listdir()
    liste_fichiers.sort()
    print(patient)
    liste_fichiers=os.listdir()
    #print(liste_fichiers)
    liste_fichiers=[x for x in liste_fichiers if str(patient)+'_'+str(mode)+'_' in x and '.csv' in x]
    #print('sortie',liste_fichiers)
    #ligne qui classe les jours dans le bon ordre
    liste_fichiers.sort()
    concatenation(liste_fichiers[0],liste_fichiers[1],'intermediaire.csv')
    for x in liste_fichiers[2:]:
        concatenation('intermediaire.csv',x,'intermediaire.csv')
    #os.system('mv intermediaire.csv '+path+str(patient)+'_complet_'+str(mode)+'.csv')
    shutil.move("intermediaire.csv",path+str(patient)+'_complet_'+str(mode)+'.csv')

##Recollement complet
#il suffit d'exécuter cette fonction pour tout obtenir (environ 20 secondes par patient mais très dépendant de la durée du séjour)
def recollement_scores_cardio(patient,numero_etude):
    x = patient
    for mode in [0,2]:
        concatenation_par_jour(patient,mode)
        concatenation_par_patient(patient,mode)
    os.chdir(path)
    with open(str(x)+'_complet_0.csv', "r",encoding="utf8", errors='ignore') as f1:
        L=[]
        lines = csv.reader(f1)
        for _ in lines:
            L.append(_)
    with open(str(x)+'_complet_2.csv', "r",encoding="utf8", errors='ignore') as f2:
        lines = csv.reader(f2)
        for _ in lines:
            L.append(_)
    with open(str(numero_etude)+'_'+str(x)+'_scores_cardio_complet.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile)
        wr.writerow(['numéro de patient Cleanweb','code clinisoft'])
        wr.writerow([str(numero_etude),str(x),'Prenez garde à bien régler le jour 1'])
        for row in L:
            wr.writerow(row)
    # une commande linux qu'on peut commenter : à modifier
    os.system('rm *_complet_*')
    shutil.rmtree("converted")

def tableau_scores_cardio_patient(patient,numero_etude):
    sans_conversion(patient)
    recollement_scores_cardio(patient,numero_etude)

#on pourra faire une boucle for pour une liste de patients, voire récupérer
#directement les numéros des fichiers téléchargés

##Essai
tableau_scores_cardio_patient(18346,28)
tableau_scores_cardio_patient(18295,84)
