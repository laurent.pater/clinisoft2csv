#  Copyright (C) 2020
#  Laurent Pater <laurent.pater@ens-cachan.org>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License version 3 or
#  above as published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#import numpy as np
import os
import csv
##Conversion en utf8 et fonction concaténation de CSV
#reglage du chemin des fichiers labo
path='/home/laurent/Bureau/COVID-ICU/a_convertir/'
os.chdir(path)
#regalge du numero de patient : pas tres utile en pratique
liste_patients=[18479, 18432, 18469, 18446, 18484, 18470, 18468, 18379, 18382, 18440, 18444]
code = dict()
code[2344]=1
code[51613]=8

#conversion en utf8 pour manipulation de fichiers non necessaire avec Selenium
# liste_fichiers=os.listdir()
# liste_fichiers = [x for x in liste_fichiers if 'labo' in x and ('01' in x or '02' in x or '03' in x or '04' in x or '07' in x or '12' in x)]
# os.system('rm -r converted/')
# os.system('mkdir converted')
# for x in liste_fichiers:
#     os.system('iconv -f ISO-8859-1 -t UTF-8 '+x+' -o '+path+'converted/'+x[:-5])

#Cette fonction colle les tableaux cote a cote : technique -> transposition
def concatenation(first,second,output):
    with open(first) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if L!=[]:
            a = max([len(x) for x in L])
        else:
            a=0
        for row in L:
            while len(row) <a:
                row.append('')
        L_trans01=list(map(list, zip(*L)))
    with open(second) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if L!=[]:
            b = b=max([len(x) for x in L])
        else:
            b=0
        for row in L:
            while len(row) <b:
                row.append('')
        L_trans02=list(map(list, zip(*L)))
    L_result=L_trans01+L_trans02
    c = max([len(x) for x in L_result])
    for x in L_result:
        while len(x) < c:
            x.append('')
    L_result=list(map(list, zip(*L_result)))
    with open(output, 'w', newline='') as myfile:
        wr = csv.writer(myfile)
        for row in L_result:
            wr.writerow(row)

##supprimer des fichiers
os.chdir(path+'converted')
liste_temp=os.listdir()
for x in liste_temp:
    if 'labo' not in x:
        os.system('rm '+x)
    if 'labo05' in x or 'labo06' in x or 'labo08' in x or 'labo09' in x or 'labo10' in x or 'labo11' in x:
        os.system('rm '+x)

##Conversion en CSV individuels
os.chdir(path+'converted')
os.system('rm *.csv')
liste_fichiers=os.listdir()
liste_fichiers=[x for x in liste_fichiers if 'labo' in x]
#liste des dates à identifier : attention date peu stable 2020 ou 20 --> absurde
liste_dates=[]
for j in ['01','02','03','04','05','06','07','08','09','10','11','12']:
    for i in ['01','02','03','04','05','06','07','08','09']+[str(x) for x in range(10,32)]:
       for k in ['20']:
           #ici reperage de la chaine de caracteres html pour trouver le nombre de sous-colonnes
           liste_dates.append('"weiss10bold">'+i+'.'+j+'.'+k)
#on recolle les tableaux 02 et 03 qui donnent la premiere colonne et les autres sans decalage
def test_tableau_valeurs(fichier):
    with open(fichier) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if len(L) < 2:
            return False
        for k in range(len(L[1])):
            if L[1][k][2] != ':':
                return False
        return True

def test_tableau_colonnes(fichier):
    with open(fichier) as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
        if len(L) < 2:
            return False
        if L[0][0] != 'Heure' or L[1][0] != 'Variables':
            return False
        return True


for x in liste_fichiers:
    print(x)
    os.system('html2csv '+x)
    liste_base=['01.csv','02.csv','03.csv','04.csv','05.csv','06.csv','07.csv','08.csv','09.csv','10.csv','11.csv','12.csv','13.csv','14.csv','15.csv','16.csv','17.csv','18.csv','19.csv','20.csv','21.csv','22.csv','23.csv','24.csv']
    liste_ici=[x for x in liste_base if x in os.listdir()]
    k=0
    for zzz in liste_ici:
        if test_tableau_valeurs(zzz):
            k+=1
            os.system('mv '+zzz+' variables'+str(k)+'.csv')
    liste_ici=[x for x in liste_base if x in liste_ici]
    for zz in liste_ici:
        if test_tableau_colonnes(zz):
            os.system('mv '+zz+' colonnes.csv')
            break
    #on recolle les variables
    os.system('mv variables1.csv variables.csv')
    for i in range(2,k+1):
        concatenation('variables.csv','variables'+str(i)+'.csv','variables.csv')
    fichier=open(x,encoding="utf8", errors='ignore')
    fichier=fichier.readlines()
    liste_cases_par_date=[]
    for _ in fichier:
        for date in liste_dates:
            z = _.find(date,0)
            if z != -1:
                a1=_.find('colspan="1"',z-158,z)
                a2=_.find('colspan="2"',z-158,z)
                a3=_.find('colspan="3"',z-158,z)
                a4=_.find('colspan="4"',z-158,z)
                a5=_.find('colspan="5"',z-158,z)
                a6=_.find('colspan="6"',z-158,z)
                if a1 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],1))
                if a2 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],2))
                if a3 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],3))
                if a4 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],4))
                if a5 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],5))
                if a6 != -1:
                    liste_cases_par_date.append((date[14:][6]+date[14:][7]+date[14:][3]+date[14:][4]+date[14:][0]+date[14:][1],date[14:],6))
    liste_cases_par_date.sort()
    liste_cases_par_date.reverse()
    premiere_ligne_tableau=[]
    for y in liste_cases_par_date:
        premiere_ligne_tableau+=[y[1]]*y[2]
    with open('variables.csv') as csvfile:
        spamreader = csv.reader(csvfile)
        L=[]
        for row in spamreader:
            L.append(row)
    with open('variables.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile)
        L[0]=premiere_ligne_tableau
        for line in L:
            line.reverse()
        for row in L:
            wr.writerow(row)
    concatenation('colonnes.csv','variables.csv',x+'.csv')

#on supprime 02 et 01 qui trainent
#os.system('rm 01.csv 02.csv')

##liste des codes
os.chdir(path+'converted')
liste_patients=os.listdir()
liste_patients=[x[2:8] for x in liste_patients]
L=[]
for x in liste_patients:
    if x not in L:
        L.append(x)
M=[]
for x in L:
    if x[0]=='_':
        M.append(int(x[1:]))
    else:
        M.append(int(x[:-1]))

liste_patients=M[:]


##Creation d'une matrice de heure/date pour horodatage continu
#liste d'horodatage : sur un an
liste_heure=[i+':'+str(j) for i in ['00','01','02','03','04','05','06','07','08','09']+[str(x) for x in range(10,24)] for j in ['00','01','02','03','04','05','06','07','08','09']+[str(x) for x in range(10,60)]]
liste_dates=[]
for j in ['01','02','03','04','05','06','07','08','09','10','11','12']:
    for i in ['01','02','03','04','05','06','07','08','09']+[str(x) for x in range(10,32)]:
       for k in ['20']:
           liste_dates.append(i+'.'+j+'.'+k)
deux_premieres_lignes=[]
for date in liste_dates:
    for k in range(1440):
        deux_premieres_lignes.append(date)
deux_premieres_lignes=[deux_premieres_lignes,liste_heure*372]

#technique : creer des matrices geantes avec toutes les minutes de tous les jours sur un an (535680 colonnes) puis recollement
for a in liste_patients:
    patient=a
    os.system('rm *geant*')
    #os.system('rm *complet*')
    os.chdir(path+'converted')
    liste_fichiers=os.listdir()
    liste_fichiers=[x for x in liste_fichiers if '.csv' in x and str(a) in x]
    liste_fichiers.sort()
    #ecriture de matrice geante pour chaque fichier
    matrice_geante=[]
    for x in liste_fichiers:
        with open(x) as csvfile:
            spamreader = csv.reader(csvfile)
            L=[]
            for row in spamreader:
                L.append(row)
            n=len(L)
            p=len(L[0])
            matrice_geante[:]=deux_premieres_lignes[:]
            for k in range(n-2):
                matrice_geante.append(['']*535680)
            for w in range(0,n):
                matrice_geante[w][0]=L[w][0]
            print(len(matrice_geante))
            for k in range(535680):
                for j in range(p):
                    if L[0][j]==matrice_geante[0][k] and L[1][j]==matrice_geante[1][k]:
                        for w in range(2,n):
                            matrice_geante[w][k]=L[w][j]
            with open(x[:-4]+'_geant.csv', 'w', newline='') as myfile:
                wr = csv.writer(myfile)
                for row in matrice_geante:
                    wr.writerow(row)

    liste_fichiers=os.listdir()
    liste_fichiers=[x for x in liste_fichiers if 'geant' in x]

    #recollement des matrices geantes
    matrice_geante=[]
    for x in liste_fichiers:
        with open(x) as csvfile:
            spamreader = csv.reader(csvfile)
            for row in spamreader:
                if 'Heure' not in row[0] and 'Variables' not in row[0]:
                    matrice_geante.append(row)

    matrice_geante=deux_premieres_lignes+matrice_geante

    #elimination des colonnes nulles dans les matrices geantes
    n = len(matrice_geante)
    p = len(matrice_geante[0])
    liste_colonnes_conservables=[]
    k=0
    for k in range(p):
        if [row[k] for row in matrice_geante[2:]] != ['']*(n-2):
            liste_colonnes_conservables.append(k)
    print('liste',liste_colonnes_conservables)

    matrice_geante_videe=[]
    for x in matrice_geante:
        L=[]
        for k in liste_colonnes_conservables:
            L.append(x[k])
        matrice_geante_videe.append(L)

    with open(str(patient)+'_labo_complet.csv', 'w', newline='') as myfile:
        wr = csv.writer(myfile)
        for row in matrice_geante_videe:
            wr.writerow(row)

    os.system('rm *geant*')

##Renommage
patient=dict()
patient[2344]=1
patient[51613]=8
os.chdir(path+'converted')
liste_temp=os.listdir()
liste_patient=list(patient)
for y in liste_temp:
    for x in liste_patient:
        if str(x) in y:
            os.system('mv '+y+' '+str(patient[x])+'_'+str(x)+'_labo_complet.csv')




##Conservation des variables souhaitees
#fonctions de filtrage basiques : a retravailler pour le fibrinogene et autres...
# os.chdir(path+'converted')
# with open(str(patient)+'_labo_complet.csv') as csvfile:
#     spamreader = csv.reader(csvfile)
#     L=[]
#     for row in spamreader:
#         L.append(row)
#
# L_filtree=[]
#
# def ajout_section(chaine,liste_depart):
#     for x in liste_depart:
#         if chaine in x[0]:
#             return(x)
#
# L_filtree.append(ajout_section('Heure',L))
# L_filtree.append(ajout_section('Variables',L))
# L_filtree.append(ajout_section('Art-PaO2',L))
# L_filtree.append(ajout_section('Art-PCO2',L))
# L_filtree.append(ajout_section('Art-HCO3',L))
# L_filtree.append(ajout_section('Lac',L))
# L_filtree.append(ajout_section('Art-pH',L))
# L_filtree.append([])
# L_filtree.append(ajout_section('TP70',L))
# L_filtree.append(ajout_section('TCA',L))
# L_filtree.append(ajout_section('Plaquettes',L))
# L_filtree.append([])
# L_filtree.append(ajout_section('GBLeucocytes',L))
# L_filtree.append(ajout_section('Neutro',L))
# L_filtree.append(ajout_section('Lympho',L))
# L_filtree.append(ajout_section('Hb12-16',L))
# L_filtree.append(ajout_section('ASAT',L))
# L_filtree.append(ajout_section('ALAT',L))
# L_filtree.append(ajout_section('CPK0-17',L))
# L_filtree.append([])
# L_filtree.append(['Variables manquantes :'])
# L_filtree.append(['CREAT (sang)'])
# L_filtree.append(['LDH'])
# L_filtree.append(['CRP'])
# L_filtree.append(['Troponine'])
# L_filtree.append(['Fibrinogene'])
# L_filtree.append(['Art-SpO2'])
#
# with open(str(patient)+'_labo_complet_simplifie.csv', 'w', newline='') as myfile:
#     wr = csv.writer(myfile)
#     for row in L_filtree:
#         wr.writerow(row)
#
# os.system('cp '+str(patient)+'_labo_complet_simplifie.csv '+path+str(patient)+'_labo_complet_simplifie.csv')

##Dossiers patients
# os.chdir('/home/laurent/Bureau/COVID-ICU/patients/')
# for x in liste_patients:
#     os.system('mkdir '+str(code[x])+'_'+str(x))
