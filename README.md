# clinisoft2csv

Some free Python codes to convert medical data from Clinisoft (TM), a software provided by General Electric, to csv files.

These codes were created during a volunteer mission at Pontoise hospital (https://www.ght-novo.fr/ch-pontoise/) and linked to the COVID-ICU study.